# frozen_string_literal: true

# == Schema Information
#
# Table name: courses
#
#  id            :bigint           not null, primary key
#  description   :string
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  discipline_id :bigint
#  teacher_id    :bigint
#
# Indexes
#
#  index_courses_on_discipline_id  (discipline_id)
#  index_courses_on_teacher_id     (teacher_id)
#
# Foreign Keys
#
#  fk_rails_...  (discipline_id => disciplines.id)
#  fk_rails_...  (teacher_id => teachers.id)
#
FactoryBot.define do
  factory :course do
    name { 'MyString' }
    description { 'MyString' }
  end
end
