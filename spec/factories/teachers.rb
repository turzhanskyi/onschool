# frozen_string_literal: true

# == Schema Information
#
# Table name: teachers
#
#  id          :bigint           not null, primary key
#  description :text
#  first_name  :string
#  last_name   :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :teacher do
    first_name { 'MyString' }
    last_name { 'MyString' }
    description { 'MyText' }
  end
end
