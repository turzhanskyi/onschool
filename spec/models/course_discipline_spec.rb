# frozen_string_literal: true

# == Schema Information
#
# Table name: course_disciplines
#
#  id            :bigint           not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  course_id     :bigint
#  discipline_id :bigint
#
# Indexes
#
#  index_course_disciplines_on_course_id      (course_id)
#  index_course_disciplines_on_discipline_id  (discipline_id)
#
# Foreign Keys
#
#  fk_rails_...  (course_id => courses.id)
#  fk_rails_...  (discipline_id => disciplines.id)
#
require 'rails_helper'

RSpec.describe CourseDiscipline, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
