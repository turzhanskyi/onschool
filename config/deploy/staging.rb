# frozen_string_literal: true

server 'staging.turvitan.km.ua', user: 'deployer', roles: %w[web app db], port: 22
set :branch, 'staging'
set :rails_env, 'staging'
set :stage, 'staging'
set :sidekiq_env, 'staging'
set :deploy_to, '/home/deployer/apps/staging.turvitan.km.ua'
