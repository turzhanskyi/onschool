# frozen_string_literal: true

Rails.application.routes.draw do
  scope :admin do
    devise_for :admins, controllers: { sessions: 'admin/admins/sessions' }
  end

  namespace :admin do
    root 'main#index'

    resources :teachers, except: :show
    resources :disciplines, except: :show

    resources :courses, except: :show do
      resources :lessons, except: :show
    end

    resources :lessons, only: [] do
      post :sort, on: :collection
    end

    namespace :api do
      namespace :lessons do
        resource :mass_update, only: :create
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
