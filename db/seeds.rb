# frozen_string_literal: true

require './db/seeds/admin'
require './db/seeds/teachers'
require './db/seeds/disciplines'
require './db/seeds/courses'
require './db/seeds/sections'
require './db/seeds/course_disciplines'
require './db/seeds/lessons'
