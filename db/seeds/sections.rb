# frozen_string_literal: true

if Section.count.zero?
  Rails.logger.info 'Seeding Sections'

  %w[1 2].each do |n|
    Section.create!(name: "Section#{n}", course_id: 1, description: n.to_s)
  end
end
