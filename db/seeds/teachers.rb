# frozen_string_literal: true

if Teacher.count.zero?
  Rails.logger.info 'Seeding Teachers'

  Teacher.create!(first_name: 'Vitalii', last_name: 'Turzhanskyi', description: 'Main Teacher')
  Teacher.create!(first_name: 'Jane', last_name: 'Dow', description: 'Third Teacher')

  5.times { |n| Teacher.create!(first_name: 'John', last_name: "Dow#{n}", description: 'Second Teacher') }
end
