# frozen_string_literal: true

if CourseDiscipline.count.zero?
  Rails.logger.info 'Seeding CourseDisciplines'

  [1, 2].each do |id|
    CourseDiscipline.create!(course_id: id, discipline_id: 1)
    CourseDiscipline.create!(course_id: id, discipline_id: 2)
  end
end
