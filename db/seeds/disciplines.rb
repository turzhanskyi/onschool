# frozen_string_literal: true

if Discipline.count.zero?
  Rails.logger.info 'Seeding Disciplines'

  %w[Html CSS Ruby RubyOnRails].each do |name|
    Discipline.create!(name: name)
  end

end
