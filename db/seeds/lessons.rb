# frozen_string_literal: true

if Lesson.count.zero?
  Rails.logger.info 'Seeding Lessons'

  %w[1 2].each do |n|
    Lesson.create!(name: "Lesson#{n}", course_id: 1, section_id: 1, description: n.to_s)
  end
end
