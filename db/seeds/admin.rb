# frozen_string_literal: true

if Admin.count.zero?
  Rails.logger.info 'Seeding Admin'
  Admin.create!(email: 'john@dow.com', first_name: 'john', last_name: 'Dow',
                password: '123123', password_confirmation: '123123')
end
