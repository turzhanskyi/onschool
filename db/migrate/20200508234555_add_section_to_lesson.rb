# frozen_string_literal: true

class AddSectionToLesson < ActiveRecord::Migration[6.0]
  change_table :lessons do |t|
    t.references :section, foreign_key: true
  end
end
