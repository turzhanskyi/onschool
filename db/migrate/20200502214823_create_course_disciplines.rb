# frozen_string_literal: true

class CreateCourseDisciplines < ActiveRecord::Migration[6.0]
  def change
    create_table :course_disciplines do |t|
      t.references :course, foreign_key: true
      t.references :discipline, foreign_key: true

      t.timestamps
    end
  end
end
