import "bootstrap";
import "../stylesheets/admin.sass";
import "@fortawesome/fontawesome-free/js/all";
import "cocoon";

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

require("jquery")
require("jquery-ui")

require("trix")
require("@rails/actiontext")

require("packs/admin/course")
require("packs/admin/lessons")

document.addEventListener("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
})