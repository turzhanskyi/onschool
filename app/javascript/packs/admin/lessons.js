var Lessons,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

$(function() {
  return new Lessons();
});

Lessons = (function() {

  function Lessons() {
    this.save = __bind(this.save, this);

    this.update = __bind(this.update, this);

    this.init_sortable = __bind(this.init_sortable, this);
    this.$sortable_container = $('.lessons_sortable');
    this.init_sortable();
  }

  Lessons.prototype.init_sortable = function() {
    var _this = this;
    return this.$sortable_container.sortable({
      axis: 'y',
      handle: '.handle',
      stop: function() {
        return _this.update();
      }
    });
  };

  Lessons.prototype.update = function() {
    var $lesson, data, lesson, lesson_id, lesson_position, position, section_id, _i, _len, _ref;
    data = [];
    _ref = this.$sortable_container.find('.lesson');
    for (position = _i = 0, _len = _ref.length; _i < _len; position = ++_i) {
      lesson = _ref[position];
      $lesson = $(lesson);
      lesson_id = $lesson.data('id');
      section_id = $lesson.prev('tr.section').data('id');
      lesson_position = position + 1;
      data.push({
        id: lesson_id,
        section_id: section_id,
        position: position + 1
      });
      $lesson.find('.position').text(lesson_position);
    }
    return this.save(data);
  };

  Lessons.prototype.save = function(data) {
    return $.ajax({
      type: "POST",
      url: this.$sortable_container.data('update-url'),
      data: {
        lessons: data
      },
      dataType: 'json'
    });
  };

  return Lessons;

})();