var Course,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

$(function() {
    return new Course();
});

Course = (function() {

    function Course() {
        this.recount_positions = __bind(this.recount_positions, this);

        this.init_sortable = __bind(this.init_sortable, this);
        this.init_sortable();
    }

    Course.prototype.init_sortable = function() {
        var _this = this;
        return $('.sections_sortable').sortable({
            axis: 'y',
            handle: '.handle',
            stop: function() {
                return _this.recount_positions();
            }
        });
    };

    Course.prototype.recount_positions = function() {
        var $inputs, input, position, _i, _len, _results;
        $inputs = $('.sections_sortable .course_sections_position input');
        _results = [];
        for (position = _i = 0, _len = $inputs.length; _i < _len; position = ++_i) {
            input = $inputs[position];
            _results.push($(input).val(position + 1));
        }
        return _results;
    };

    return Course;

})();
