# frozen_string_literal: true

# == Schema Information
#
# Table name: disciplines
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Discipline < ApplicationRecord
  has_many :course_disciplines, dependent: :destroy
  has_many :courses, through: :course_disciplines

  validates :name, presence: true
end
