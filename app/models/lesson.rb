# frozen_string_literal: true

# == Schema Information
#
# Table name: lessons
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :string
#  position    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  course_id   :bigint           not null
#  section_id  :bigint
#
# Indexes
#
#  index_lessons_on_course_id   (course_id)
#  index_lessons_on_section_id  (section_id)
#
# Foreign Keys
#
#  fk_rails_...  (course_id => courses.id)
#  fk_rails_...  (section_id => sections.id)
#
class Lesson < ApplicationRecord
  belongs_to :course
  belongs_to :section

  has_rich_text :description

  validates :name, :description, presence: true

  acts_as_list

  def self.reorder(order_params)
    order_params.each_with_index do |id, index|
      Lesson.find(id).update!(position: index + 1)
    end
  end
end
