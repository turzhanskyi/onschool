# frozen_string_literal: true

# == Schema Information
#
# Table name: teachers
#
#  id          :bigint           not null, primary key
#  description :text
#  first_name  :string
#  last_name   :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Teacher < ApplicationRecord
  has_many :courses, dependent: :destroy

  validates :first_name, :last_name, :description, presence: true
end
