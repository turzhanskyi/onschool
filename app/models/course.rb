# frozen_string_literal: true

# == Schema Information
#
# Table name: courses
#
#  id            :bigint           not null, primary key
#  description   :string
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  discipline_id :bigint
#  teacher_id    :bigint
#
# Indexes
#
#  index_courses_on_discipline_id  (discipline_id)
#  index_courses_on_teacher_id     (teacher_id)
#
# Foreign Keys
#
#  fk_rails_...  (discipline_id => disciplines.id)
#  fk_rails_...  (teacher_id => teachers.id)
#
class Course < ApplicationRecord
  belongs_to :teacher

  has_many :lessons, dependent: :destroy
  has_many :sections, dependent: :destroy
  has_many :course_disciplines, dependent: :destroy
  has_many :disciplines, through: :course_disciplines

  has_rich_text :description

  has_one_attached :course_image
  validate :acceptable_image

  validates :name, presence: true

  accepts_nested_attributes_for :sections, reject_if: :all_blank, allow_destroy: true

  def acceptable_image
    return unless course_image.attached?

    errors.add(:course_image, 'is too big') unless course_image.byte_size <= 1.megabyte

    acceptable_types = %w[image/jpeg image/png]
    errors.add(:course_image, 'must be a JPEG or PNG') unless acceptable_types.include?(course_image.content_type)
  end
end
