# frozen_string_literal: true

class Admin::LessonsController < Admin::BaseController
  before_action :set_course
  before_action :set_lesson, only: %i[edit update destroy]

  def index
    @lessons = @course.lessons.order(:position).page(params[:page])
  end

  def new
    add_breadcrumb 'Нове заняття', [:new, :admin, @course, :lesson]

    @lesson = @course.lessons.build
  end

  def create
    @lesson = @course.lessons.build(lesson_params)

    respond_to do |format|
      if @lesson.save
        flash[:notice] = 'Заняття успішно створено'
        format.html { redirect_to [:admin, @course, :lessons] }
        format.json { render :index, status: :created, location: @lesson }
      else
        add_breadcrumb 'Нове заняття', [:new, :admin, @course, :lesson]

        flash[:alert] = 'Не вдалося створити заняття'
        format.html { render :new }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    add_breadcrumb "Редагувати #{@lesson.name}", [:edit, :admin, @course, :lesson]
  end

  def update
    respond_to do |format|
      if @lesson.update(lesson_params)
        flash[:notice] = 'Дані заняття оновлено'
        format.html { redirect_to [:admin, @course, :lessons] }
        format.json { render :index, status: :ok, location: @lesson }
      else
        add_breadcrumb "Редагувати #{@lesson.name}", [:edit, :admin, @course, :lesson]

        flash[:alert] = 'Не вдалося оновити дані заняття'
        format.html { render :edit }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lesson.destroy
    respond_to do |format|
      flash[:notice] = 'Заняття успішно видалено'
      format.html { redirect_to [:admin, @course, :lessons] }
      format.json { head :no_content }
    end
  end

  private

  def set_course
    @course = Course.find(params[:course_id])

    add_breadcrumb 'Курси', :admin_courses_path
    add_breadcrumb @course.name, [:admin, @course, :lessons]
  end

  def set_lesson
    @lesson = Lesson.find(params[:id])
  end

  def set_active_main_item
    @main_menu[:courses][:active] = true
  end

  def lesson_params
    params.require(:lesson).permit(:name, :description, :section_id)
  end
end
