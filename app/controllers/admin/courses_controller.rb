# frozen_string_literal: true

class Admin::CoursesController < Admin::BaseController
  add_breadcrumb 'Курси', :admin_courses_path

  before_action :set_course, only: %i[edit update destroy]

  def index
    @courses = Course.order(id: :asc).page(params[:page])
  end

  def new
    add_breadcrumb 'Новий курс', new_admin_course_path

    @course = Course.new
    build_sections
  end

  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        flash[:notice] = 'Курс успішно створено'
        format.html { redirect_to admin_courses_path }
        format.json { render :index, status: :created, location: @course }
      else
        add_breadcrumb 'Новий курс', new_admin_course_path
        build_sections

        flash[:alert] = 'Не вдалося створити курс'
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @course.sections.build if @course.sections.empty?

    add_breadcrumb "Редагувати #{@course.name}", [:edit, :admin, @course]
  end

  def update
    respond_to do |format|
      if @course.update(course_params)
        flash[:notice] = 'Дані курсу оновлено'
        format.html { redirect_to admin_courses_path }
        format.json { render :index, status: :ok, location: @course }
      else
        add_breadcrumb "Редагувати #{@course.name}", [:edit, :admin, @course]
        build_sections

        flash[:alert] = 'Не вдалося оновити дані курсу'
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @course.destroy
    respond_to do |format|
      flash[:notice] = 'Курс успішно видалено'
      format.html { redirect_to admin_courses_path }
      format.json { head :no_content }
    end
  end

  private

  def set_course
    @course = Course.find(params[:id])
  end

  def set_active_main_item
    @main_menu[:courses][:active] = true
  end

  def build_sections
    @course.sections.build if @course.sections.empty?
  end

  def course_params
    params.require(:course).permit(:name, :description, :teacher_id, :course_image, discipline_ids:      [],
                                                                                    sections_attributes: %i[_destroy id name description position])
  end
end
