# frozen_string_literal: true

class Admin::DisciplinesController < Admin::BaseController
  add_breadcrumb 'Дисципліни', :admin_disciplines_path

  before_action :set_discipline, only: %i[edit update destroy]

  def index
    @disciplines = Discipline.order(id: :desc).page(params[:page])
  end

  def new
    add_breadcrumb 'Нова дисципліна', new_admin_discipline_path

    @discipline = Discipline.new
  end

  def create
    @discipline = Discipline.new(discipline_params)

    respond_to do |format|
      if @discipline.save
        flash[:notice] = 'Дисципліна успішно створена'
        format.html { redirect_to admin_disciplines_path }
        format.json { render :index, status: :created, location: @discipline }
      else
        add_breadcrumb 'Нова дисципліна', new_admin_discipline_path

        flash[:alert] = 'Не вдалося створити дисципліну'
        format.html { render :new }
        format.json { render json: @discipline.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    add_breadcrumb "Редагувати #{@discipline.name}", [:edit, :admin, @discipline]
  end

  def update
    respond_to do |format|
      if @discipline.update(discipline_params)
        flash[:notice] = 'Дані про дисципліну оновлено'
        format.html { redirect_to admin_disciplines_path }
        format.json { render :index, status: :ok, location: @discipline }
      else
        add_breadcrumb "Редагувати #{@discipline.name}", [:edit, :admin, @discipline]

        flash[:alert] = 'Не вдалося оновити дані про дисципліну'
        format.html { render :edit }
        format.json { render json: @discipline.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @discipline.destroy
    respond_to do |format|
      flash[:notice] = 'Дисципліну успішно видалено'
      format.html { redirect_to admin_disciplines_path }
      format.json { head :no_content }
    end
  end

  private

  def set_discipline
    @discipline = Discipline.find(params[:id])
  end

  def set_active_main_item
    @main_menu[:disciplines][:active] = true
  end

  def discipline_params
    params.require(:discipline).permit(:name)
  end
end
