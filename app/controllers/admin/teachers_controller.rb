# frozen_string_literal: true

class Admin::TeachersController < Admin::BaseController
  add_breadcrumb 'Викладачі', :admin_teachers_path

  before_action :set_teacher, only: %i[edit update destroy]

  def index
    @teachers = Teacher.order(id: :desc).page(params[:page])
  end

  def new
    add_breadcrumb 'Новий викладач', new_admin_teacher_path

    @teacher = Teacher.new
  end

  def create
    @teacher = Teacher.new(teacher_params)

    respond_to do |format|
      if @teacher.save
        flash[:notice] = 'Викладач успішно створений'
        format.html { redirect_to admin_teachers_path }
        format.json { render :index, status: :created, location: @teacher }
      else
        add_breadcrumb 'Новий викладач', new_admin_teacher_path

        flash[:alert] = 'Не вдалося створити викладача'
        format.html { render :new }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    add_breadcrumb "Редагувати #{@teacher.decorate.full_name}", [:edit, :admin, @teacher]
  end

  def update
    respond_to do |format|
      if @teacher.update(teacher_params)
        flash[:notice] = 'Дані викладача оновлено'
        format.html { redirect_to admin_teachers_path }
        format.json { render :index, status: :ok, location: @teacher }
      else
        add_breadcrumb "Редагувати #{@teacher.decorate.full_name}", [:edit, :admin, @teacher]

        flash[:alert] = 'Не вдалося оновити дані викладача'
        format.html { render :edit }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @teacher.destroy
    respond_to do |format|
      flash[:notice] = 'Успішно викладача видалено'
      format.html { redirect_to admin_teachers_path }
      format.json { head :no_content }
    end
  end

  private

  def set_teacher
    @teacher = Teacher.find(params[:id])
  end

  def set_active_main_item
    @main_menu[:teachers][:active] = true
  end

  def teacher_params
    params.require(:teacher).permit(:first_name, :last_name, :description)
  end
end
