# frozen_string_literal: true

class Admin::BaseController < ApplicationController
  layout 'admin'

  before_action :authenticate_admin!
  before_action :set_main_menu
  before_action :set_active_main_item

  private

  def set_main_menu
    @main_menu = { courses:     { name: 'Курси', path: admin_courses_path },
                   disciplines: { name: 'Дисципліни', path: admin_disciplines_path },
                   teachers:    { name: 'Викладачі', path: admin_teachers_path } }
  end
end
